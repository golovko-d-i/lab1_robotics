package by.bsh.lab1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MainActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2 {

    public static final String TAG = MainActivity.class.getSimpleName();

    private JavaCameraView mCameraView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mCameraView = (JavaCameraView) findViewById(R.id.cameraView);
        if (mCameraView != null) {
            mCameraView.setCvCameraViewListener(this);
        }
    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {

        Mat gray = inputFrame.gray();

        // compute the Scharr gradient magnitude representation of the images
        // in both the x and y direction
        Mat gradX = new Mat();
        Mat gradY = new Mat();

        Imgproc.Sobel(gray, gradX, CvType.CV_32F, 1, 0, -1, 1f, 0, 0);
        Imgproc.Sobel(gray, gradY, CvType.CV_32F, 0, 1, -1, 1f, 0, 0);

        // subtract the y-gradient from the x-gradient
        Mat gradient = new Mat(gray.rows(), gray.cols(), CvType.CV_32F);
        Core.subtract(gradX, gradY, gradient);

        gradX.release();
        gradY.release();

        Mat gradient2 = new Mat();

        Core.convertScaleAbs(gradient, gradient2);
//
        gradient.release();

        // blur and threshold the image
        Mat blurred = new Mat();
        Imgproc.blur(gradient2, blurred, new Size(9, 9));
//
        gradient2.release();
//
        Mat tresholded = new Mat();

        Imgproc.threshold(blurred, tresholded, 225, 255, Imgproc.THRESH_BINARY);
        blurred.release();

        // construct a closing kernel and apply it to the thresholded image
        Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(21, 7));

        Mat closed = new Mat();
        Imgproc.morphologyEx(tresholded, closed, Imgproc.MORPH_CLOSE, kernel);
//
        tresholded.release();

        // perform a series of erosions and dilations
        Mat erode = new Mat();
        Imgproc.erode(closed, erode, new Mat(), new Point(), 4);

        Mat dilate = new Mat();
        Imgproc.dilate(erode, dilate, new Mat(), new Point(), 4);

        erode.release();

        // find the contours in the thresholded image, then sort the contours
        // by their area, keeping only the largest one
        ArrayList<MatOfPoint> contours = new ArrayList<>();
        Imgproc.findContours(dilate, contours, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);

        dilate.release();

        if (contours.isEmpty()) {
            return inputFrame.rgba();
        }

        Collections.sort(contours, new Comparator<MatOfPoint>() {
            @Override
            public int compare(MatOfPoint lhs, MatOfPoint rhs) {
                return Double.compare(Imgproc.contourArea(lhs), Imgproc.contourArea(rhs));
            }
        });

        MatOfPoint max = contours.get(contours.size() - 1);

        try {

            // compute the rotated bounding box of the largest contour
            RotatedRect rect = Imgproc.minAreaRect(new MatOfPoint2f(max.toArray()));
            Rect r = rect.boundingRect();
            Mat rgb = inputFrame.rgba();

            Core.rectangle(rgb, r.br(), r.tl(), new Scalar(0, 255, 0), 3);

            return rgb;

        } catch (Exception e) {
            e.printStackTrace();
            return inputFrame.rgba();
        }
    }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mCameraView.enableView();
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    @Override
    public void onPause() {
        super.onPause();

        if (mCameraView != null) {
            mCameraView.disableView();
        }

    }

    @Override
    public void onResume() {
        super.onResume();

        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_11
                    , this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }

    }

    @Override
    public void onCameraViewStarted(int width, int height) {

    }

    @Override
    public void onCameraViewStopped() {

    }
}
